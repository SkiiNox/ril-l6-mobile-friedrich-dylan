var app = angular.module('app');

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider

        .state('home', {
            url: '/countries',
            controller: 'Countries',
            templateUrl: 'webpages/countries.html'
        })

        .state('countryDetail', {
            url: '/countries/:iso2',
            controller: 'countryDetailCTR',
            templateUrl: 'webpages/countryDetail.html'
        })

        .state('stationDetail', {
            url: '/countries/:idContrat',
            controller: 'stationDetailCTR',
            templateUrl: 'webpages/stationDetail.html'
        })
        .state('location', {
            url: '/location',
            controller: 'locationUser',
            templateUrl: 'webpages/location.html'
        })
});
