var app = angular.module('app')

app.service('countriesService', function ($http, apiHost) {
    this.getList = function (){
        return $http.get(apiHost + 'countries');
    };
});
app.service('contractService', function ($http, apiHost) {
    this.getList = function ($iso2){
        return $http.get(apiHost + 'countries/' + $iso2+'/contracts');
    };
});

app.service('stationService', function ($http, apiHost) {
    this.getList = function ($idContrat){
        return $http.get(apiHost + 'contracts/' + $idContrat+'/stations');
    };
});

app.service('localisationService', function ($http, apiHost) {
    this.getList = function (){
        return $http.get(apiHost + 'contracts');
    };
});
