var app = angular.module('app');

// creation du controleur
app.controller('Countries', function($scope, countriesService) {

 countriesService.getList()
	 .then (function (response){
		console.log(response.data);

		$scope.countries = response.data;
	})
 	.catch(function(error) {
		console.error(error.message)
	});

});

app.controller('countryDetailCTR', function($scope, $stateParams, contractService) {

 contractService.getList($stateParams.iso2)
	 .then (function (response){
		console.log(response.data);

		$scope.contracts = response.data;
	})
 	.catch(function(error) {
		console.error(error.message)
	});

});

app.controller('stationDetailCTR', function($scope, $stateParams, stationService, $timeout) {

 stationService.getList($stateParams.idContrat)
	 .then (function (response){
		var tick = function(){
			console.log('refresh');
			$scope.stations = response.data;
			$timeout(tick, 10000)
		}
		$scope.stations = response.data;
    $scope.date = new Date();
		$timeout(tick, 10000);
		$scope.goBack = function() {
	    console.log('Going back');
	    $ionicViewService.getBackView().go();
  	}
	})
 	.catch(function(error) {
		console.error(error.message)
	});

});

app.controller('locationUser', function($scope, $state,$cordovaGeolocation,localisationService) {
    var options = {timeout: 10000, enableHighAccuracy: true};

  $cordovaGeolocation.getCurrentPosition(options).then(function(position){

		$scope.latitude = position.coords.latitude;
		$scope.longitude= position.coords.longitude;

			 localisationService.getList()
				 .then (function (response){
					 //on recupere nos contrats et dans
					 //On compare nos lattitude
					 console.log(response.data);
					 $scope.localisationPos = response.data;
					 	//google.maps.geometry.spherical.computeDistanceBetween(latLng, latLngObject)
			  	})
			 	.catch(function(error) {
					console.error(error.message)
				});

  }, function(error){
    console.log('Localisation indisponible');
  });

});
